$(function(){
    var flight_destination = $('#destination'),
        flight_departure = $('#departure'),
        flight_arrival = $('#arrival'),
        flight_number = $('#flight_number');

    $(document).on("ready", function() {
        $.ajax({

            url:"getFlightData",
            type:"get",
            dataType:"json",
            data: { pageload:true }

        }).done(function(data) {

             for (var x in data) {

                var option = document.createElement("option");
                document.getElementById("flight_number").appendChild(option);
                $(option).val(data[x].flight_number).text(data[x].flight_number);

            }
        }).fail(function(jqXhr, err) {
            console.error(err);
        });

    });

     flight_number.on('change', function() {
        var flight_info = $(this).text();
        $.ajax({
            url: "getFlightData.php",
            type: "get",
            dataType: "json",
            data: { 'flight_number' : $(this).val() }
        })
            .done(function(data) { // this is the Promise - also known as the Deferred :)
                if (data.length > 0 && data[0]) {
                    data = data[0];
                }

                flight_destination.text(data.destination).val(data.destination);
                flight_departure.val(date("Y-m-d\\TH:i:s", data.departure));
                flight_arrival.val(date("Y-m-d\\TH:i:s", data.arrival));

            });
    });
});